import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { getConfig } from "./config";
import {TruIAMProvider} from "truiam-react";


const config = getConfig();

const providerConfig = {
  domain: config.domain,
  clientId: config.clientId,
  redirectUri: window.location.origin
};

ReactDOM.render(
  <TruIAMProvider {...providerConfig}>
    <App />
  </TruIAMProvider>,
  document.getElementById("root")
);

