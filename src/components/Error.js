import React from "react";
import errorImage from "../assets/error-image.png";

import { truIAMLib } from "truiam-react";


const Error = () => {
  const {error} = truIAMLib();
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        height: "100vh",
        textAlign: "center",
      }}
    >
      <img
        src={errorImage}
        alt="Error"
        style={{ width: "200px", marginBottom: "20px" }}
      />
      <h1
        style={{
          fontSize: "28px",
          fontWeight: "bold",
          marginBottom: "10px",
          textTransform: "uppercase",
        }}
      >
        Oops... An Error Occurred
      </h1>
      <h2 style={{ fontSize: "24px", marginBottom: "20px", color: "#f44336" }}>
        {error.error.toUpperCase()}
      </h2>
      <p style={{ fontSize: "18px", lineHeight: "24px", marginBottom: "40px" }}>
        {error.description}
      </p>
    </div>
  );
};

export default Error;
